#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'GLUG-PESCE'
SITENAME = u"GLUG-PESCE's Blog"
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('GLUG-PESCE Website', 'https://glugpesce.github.io'),('PESCE Website', 'http://pescemandya.org/'),
         ('FSMK Website', 'http://fsmk.org/'),)

# Social widget
SOCIAL = (('GLUG-PESCE Website', 'https://glugpesce.github.io'),('Mastodon', '@glugpesce@mastodon.social'),
          ('Twitter', 'twitter.com/glugpesce@9'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

OUTPUT_PATH = 'public/'

# THEME CONFIG
THEME = 'pelican-bootstrap3'
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGIN_PATHS = ['plugins']
PLUGINS = ['i18n_subsites']
BANNER = 'assets/banner.png'
BANNER_SUBTITLE = "Welcome to GLUG-PESCE Blog Post."
STATIC_PATHS = ['assets']
